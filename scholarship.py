"""
A student is eligible for scholarships if he satisfies the below conditions:
A. Student is between the  age 18 and 24 (boundary value included)
B. Student has lived in California for last 2 years, if he fails this criterion, check if satisfies D.
C. Has worked part time for at least for 6 months in the relevant field of study, if he fails this criterion, check if satisfies E.
D. Parents of the student have paid California state tax for at least 1 year in their lifetime.
E. Has volunteered for a cause and has a valid proof of it.
F. Has household income less than 5000$ per annum then one need not satisfy criteria C, he will be redirected to "Dean for consideration"
"""
import re

if __name__ == "__main__":
    eligible = False
    age = 0
    residency = False
    work = False
    taxes = False
    volunteer = False
    income = 10000
    print("Input Student age")
    input_age = input()
    if input_age.isdigit():
        age = int(input_age)
    print("Has the student lived in California for the past two years? (y/n)")
    input_residency = input()
    if input_residency in "Yesyes":
        residency = True
    else:
        residency = False
    print("Has the student months worked part time in field? (y/n)")
    input_work = input()
    if input_work in "Yesyes":
        work = True
    else:
        work = False
    print("Has the student or student's family filed for California state taxes? (y/n)")
    input_tax = input()
    if input_tax in "Yesyes":
        tax = True
    else:
        tax = False
    print("Has the student volunteered? (y/n)")
    input_volunteer = input()
    if input_volunteer in "Yesyes":
        volunteer = True
    else:
        volunteer = False
    print("What is student's or family's household income")
    input_income = input()
    if re.match(r'\$?\d+(?:[.]\d{2})?$', input_income) is not None:
        income = float(input_income.strip('$'))
    if ((age >= 18 and age <= 24) and (residency or tax) and (work or volunteer) and income <= 5000):
        print("Student is eligble")
    else:
        print("Student is not eligible")

