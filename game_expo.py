
def storytelling():
    """Storytelling activity for child
    :returns: error code

    """
    print("Child signed up for storytelling")
    return 0

def drawing():
    """Drawing activity for child

    :returns: error code

    """
    print("Child signed up for drawing")
    return 0

def quiz():
    """Quiz activity for child

    :returns: error code

    """
    print("Child signed up for quizes")
    return 0

def essay_writing():
    """Essay writing activity for child

    :returns: error code

    """
    print("Child signed up for essay writing")
    return 0

def rhyming():
    """Rhyming activity for child

    :returns: error code

    """
    print("Child signed up for rhyming")
    return 0

def poetry():
    """Poetry activity for child

    :returns: error code

    """
    print("Child signed up for poetry")
    return 0

if __name__ == "__main__":
    age = -1
    gender = ""
    print("How many years old is the child?")
    age_input = input()
    if age_input.isdigit():
        age = int(age_input)
    else:
        print("Unexpected input")
        return 0
    print("Is he child a boy or a girl? (b/g)")
    gender = input()
    if gender in "Boyboy":
        if age  > 20:
            poetry()
        elif age > 11 and age < 15:
            quiz()
        elif age > 7 and age < 10:
            storytelling()
        elif age  < 6:
            rhyming()
        else:
            print("No activity for a child at this age")
    elif gender in "Girlgirl":
        if age  > 20:
            poetry()
        elif age > 10 and age < 15:
            essay_writing()
        elif age > 7 and age < 10:
            drawing()
        elif age  < 6:
            rhyming()
        else:
            print("No activity for a child at this age")
    else:
        print("Unexpected input")
    return 0
