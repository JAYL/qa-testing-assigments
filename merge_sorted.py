
if __name__ == "__main__":
    num1_index = 0
    num2_index = 0
    nums1 = [1,2,3]
    nums2 = [2,5,6,7,8,9,11,22,155]
    result = []
    for i in range(len(nums1) + len(nums2)):
        if num1_index == len(nums1):
            result.extend(nums2[num2_index:])
            break
        if num2_index == len(nums2):
            result.extend(num1[num1_index:])
            break
        if nums1[num1_index] <= nums2[num2_index]:
            result.append(nums1[num1_index])
            num1_index += 1
        else:
            result.append(nums2[num2_index])
            num2_index += 1
    print(result)

