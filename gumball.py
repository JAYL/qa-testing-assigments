"""
Design a Gumball Vending machine with the following specifications.
Machine has two types of gumballs : Red and Yellow. Red ones are worth a nickel and Yellow ones a dime.
Customer can insert only nickel, dimes or quarters as valid currency. Anything other than that is returned on the push of the dispenses lever.
There are two levers Red and Yellow to dispense respective type of gumballs.
Customer can insert multiple coins until he chooses to hit on the dispenser lever.
Machine vends a single gumball at a time.
Customer can enter a quarter, choose to dispense two Red gumballs( by hitting the red gumball dispenser lever twice) and hit a "Return My Change" lever to receive the 15 cents in return.
**Assume the machine holds unlimited gumballs and unlimited change of currency to return.
"""

"""
Function to return the largest amount of change given some input of cents
param-cents: numeric cash value that needs to be converted to available coins. Expected to be a multiple of 5
Returns: nothing function will just print out the amount of Quarters, Dimes and Nickles
"""
def currency_in_coins(cents):
    quarters = 0
    dimes = 0
    nickles = 0
    while cents != 0:
        if cents >= 25:
            quarters += 1
            cents -= 25
        elif cents >= 10:
            dimes += 1
            cents -= 10
        elif cents >= 5:
            nickles += 1
            cents -= 5
    print("Quarters: ", quarters)
    print("Dimes: ", dimes)
    print("Nickles: ", nickles)


if __name__ == "__main__":
    coin_input = 0
    currency_exchange = {"n":5, "N":5, "d":10,"D":10,"q":25,"Q":25, "s":0, "S":0}
    user_input = "z"
    while user_input not in "sS":
        #User can add nickel, dime or quarter to the machine
        print("Current Balance: ", coin_input)
        print("Nickel (n)")
        print("Dime (d)")
        print("Quarter (q)")
        print("Dispense (s)")
        user_input = input("Insert: ")
        if user_input in currency_exchange:
            #Checks if there was a valid input e.g. no pennies were added
            #invalid inputs will be ignored
            coin_input += currency_exchange[user_input]
    while(user_input not in "qQ"):
        print("push a lever")
        print("Red (r)")
        print("Yellow (y)")
        print("Refund (q)")
        print("Balance available: ", coin_input)
        #User can choose to pull for a yellow gumbal which costs 5 cents or a red gumball which casts 10 cents
        #The user also has the option to refund the remaining amount of money
        user_input = input()
        if user_input in "Rr" and coin_input >= 5:
            print("Red Gumball Dispensed")
            coin_input -= 5
        if user_input in "Yy" and coin_input >= 10:
            print("Yellow Gumball Dispensed")
            coin_input -= 10
    print("Machine returns", coin_input, "cents")
    currency_in_coins(coin_input)
    #print("Machine out of Service")


